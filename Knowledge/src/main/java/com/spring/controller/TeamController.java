package com.spring.controller;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
// import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.spring.model.team.Team;
import com.spring.team.repository.TeamRepository;

import java.util.*;

@RestController
public class TeamController {
    @Autowired
    private TeamRepository teamRepo;
       
    @GetMapping("/teams")
    public List<Team> listAll(Model model) {
        List<Team> teamLists = teamRepo.findAll();
        // model.addAttribute("teamLists", teamLists);
        
        // System.out.println(model.getAttribute("teamLists"));
        // System.out.println(teamLists);
        // teamLists.forEach(System.out :: println);
        return teamLists;
    }
    
    @PostMapping("/team/create/{name}/{member}")
    public Team createTeam(@PathVariable("name") String name, @PathVariable("member") int member) {
    	Team teamOne = new Team();
    	teamOne.setName(name);
    	teamOne.setMember(member);
    	
    	teamRepo.save(teamOne);
        return teamOne;
    }
    
    @PutMapping("/team/edit/{id}/{name}/{member}")
    public Team updateTeam(@PathVariable("id") int id,@PathVariable("name") String name, @PathVariable("member") int member) {
    	Team teamOne = teamRepo.findById(id).get();
    	teamOne.setName(name);
    	teamOne.setMember(member);
    	
    	teamRepo.save(teamOne);
        return teamOne;
    }
    
    @DeleteMapping("/team/delete/id/{id}")
    public String deleteTeamById(@PathVariable("id") int teamId) {
    	teamRepo.deleteById(teamId);
        return "1 row Deleted Successfully";
    }
    
    @Transactional
    @DeleteMapping("/team/delete/name/{name}")
    public String deleteTeamByName(@PathVariable("name") String teamName) {
    	teamRepo.deleteByName(teamName);
        return "1 row Deleted Successfully";
    }
       
}

