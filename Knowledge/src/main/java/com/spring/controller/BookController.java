package com.spring.controller;

import org.springframework.beans.factory.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.spring.model.book.Book;
import com.spring.book.repository.BookRepository;

import java.util.*;

@RestController
public class BookController {
	@Autowired
    private BookRepository bookRepo;
       
    @GetMapping("/books")
    public List<Book> listAll(Model model) {
        List<Book> bookLists = bookRepo.findAll();
        
        return bookLists;
    }
}
