package com.spring.team.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.model.team.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {
	
	long deleteByName(String name);
	Team findTopByOrderByIdDesc();
}

