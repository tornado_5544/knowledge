package com.spring;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spring.book.repository.BookRepository;
import com.spring.model.book.Book;
import com.spring.team.repository.TeamRepository;


import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.jdbc.core.JdbcTemplate;
// import org.springframework.boot.CommandLineRunner;

// public class KnowledgeApplication implements CommandLineRunner {
	
@SpringBootApplication
public class KnowledgeApplication {
	
	// @Autowired
    // private JdbcTemplate jdbcTemplate;

	@Autowired
	private BookRepository bookRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(KnowledgeApplication.class, args);
	}
	
	/* @Override
    public void run(String... args) throws Exception {
        String sql = "INSERT INTO team (name, member) VALUES ("
                + "'Python', 50)";
         
        int rows = jdbcTemplate.update(sql);
        if (rows > 0) {
            System.out.println("A new row has been inserted.");
        }
	} */
}
