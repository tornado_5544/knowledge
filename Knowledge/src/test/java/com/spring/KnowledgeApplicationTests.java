package com.spring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.spring.model.team.Team;
import com.spring.team.repository.TeamRepository;

@SpringBootTest
class KnowledgeApplicationTests {
	protected MockMvc mvc;
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
    public TeamRepository teamRepo;
	
	@Test
	void show() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		String uri = "/teams";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
		      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
 
	@Test
	void create() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		String uri = "/team/create/Raspberry/87";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
		      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		//System.out.println(content);
		Team recent = teamRepo.findTopByOrderByIdDesc();
		//System.out.println(content);
		//System.out.println(recent.toString());
		assertEquals(content, recent.toString());
	}
	
	@Test
	void edit() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		String uri = "/team/edit/20/Raspberry pi/87";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
		      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Team recent = new Team();
		recent.setId(20);
    	recent.setName("Raspberry pi");
    	recent.setMember(87);
		assertEquals(content, recent.toString());
	}

}
